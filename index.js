

function testMessage(request, sender, sendResponse) {
  let title  =  document.querySelector("title");
  let keywords = document.querySelector("meta[name='keywords']");
  let description = document.querySelector("meta[name='description']");

  console.log("title - " + title.innerHTML);
  console.log("description - " + description.getAttribute("content"));
  console.log("keywords - " + keywords.getAttribute("content"));
}


browser.runtime.onMessage.addListener(testMessage);