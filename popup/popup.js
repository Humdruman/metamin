document.addEventListener("click", (e) => {
  // Активная вкладка
  browser.tabs.executeScript(null, {
    file: "/index.js"
  });

  let gettingActiveTab = browser.tabs.query({active: true, currentWindow: true});

  if(e.target.id === "test"){

    gettingActiveTab.then((tabs) => {
      let thisTab = tabs[0].id;
      let response = {};
      browser.tabs.sendMessage(thisTab, response);
    });
  }

});